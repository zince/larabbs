<?php

namespace App\Http\Middleware;

use Closure;

class PerformanceDebug
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

//        if (app()->isLocal()){
//            $num = count(get_included_files());
//
//            dd($num);
//        }

        return $response;
    }
}

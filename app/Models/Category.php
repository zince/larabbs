<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillabled = ['name','description'];

    public function topic()
    {
        return $this->hasOne(Topic::class,'category_id','id');
    }
}
